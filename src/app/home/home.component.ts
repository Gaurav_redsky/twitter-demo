import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, LoadEventData, WebView } from "@nativescript/core";
import { firebase } from "@nativescript/firebase";
@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    lists: any;
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.lists = [
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
            `https://www.youtube.com/watch?v=wRHlgpfV4Wo`,
        ]
        // Init your component properties here.

    }
    onLogin() {
        firebase
            .login({ type: firebase.LoginType.TWITTER })
            .then(result => {
                console.log("logi result", result)
            }).catch((error) => {
                console.log("error", error)
            })
    }
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onLoadFinished(args: LoadEventData) {
        const webView = args.object as WebView;



        // if (!args.error) {
        //     console.log("Load Finished");
        //     console.log(`EventName: ${args.eventName}`);
        //     console.log(`NavigationType: ${args.navigationType}`);
        //     console.log(`Url: ${args.url}`);
        // } else {
        //     console.log(`EventName: ${args.eventName}`);
        //     console.log(`Error: ${args.error}`);
        // }
    }

}
